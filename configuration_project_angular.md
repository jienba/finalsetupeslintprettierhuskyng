# Configuration ESLint, Prettier et Husky pour un projet Angular

## Table des matières

* [Introduction](#introduction)
* [Prérequis](#pr%C3%A9requis)
* [ESLint](#eslint)
* [Prettier](#prettier)
* [Combinaison ESLint & Prettier](#combinaison-eslint--prettier)
* [Husky](#husky)
* [Analyse basé sur le nouveau code](#analyse-base-sur-le-nouveau-code)
* [Conclusion](#conclusion)
* [Annexe](#annexe)

## Introduction

Dans le développement logiciel, la cohérence et la qualité du code sont essentielles pour garantir la maintenabilité et la collaboration efficace au sein d'une équipe. Ce guide détaille la configuration d'ESLint, Prettier et Husky dans un projet Angular, offrant une solution complète pour le formatage automatique et la détection d'erreurs dans votre code.

## Prérequis

Avant de commencer à configurer ESLint, Prettier et Husky dans votre projet Angular, assurez-vous d'avoir les éléments suivants installés :

- **Node.js & npm :**

> Assurez-vous d'avoir Node.js installé sur votre système. Vous pouvez le télécharger à partir du site officiel : [Node.js](https://nodejs.org/fr).

- **Git**

> Git est un système de contrôle de version largement utilisé. Assurez-vous d'avoir Git installé et configuré sur votre système. Vous pouvez le télécharger à partir du site officiel : [Git](https://git-scm.com/).

- **Angular-CLI**

> Angular CLI est un ensemble d'outils en ligne de commande pour simplifier le processus de développement avec Angular. Vous pouvez l'installer globalement en utilisant npm :

```npm
npm install -g @angular/cli
```

- **Projet Angular**

> Assurez-vous d'avoir un projet Angular existant. Si ce n'est pas le cas, vous pouvez en créer un en utilisant Angular CLI :

---

```npm
ng new nom-du-projet
```

- **Éditeur de texte ou IDE :**

> Utilisez un éditeur de texte ou un environnement de développement intégré (IDE) de votre choix, tel que Visual Studio Code, Atom ou WebStorm.

Assurez-vous que votre projet est initialisé avec Git et Angular CLI, car Husky utilise les hooks Git pour automatiser les tâches avant chaque commit.

## ESLint

### C'est quoi un linter?

Un linter est un outil essentiel en développement logiciel. Il analyse le code source d'un programme et signale les erreurs de syntaxe, les violations des conventions de codage et d'autres problèmes potentiels. En examinant le code, le linter garantit la cohérence, améliore la lisibilité et prévient les erreurs. Les linters sont cruciaux pour maintenir un code de haute qualité, en aidant les développeurs à repérer et à corriger les problèmes avant la compilation ou l'exécution du code.

### Installation

Pour intégrer ESLint à votre projet Angular, exécutez simplement la commande suivante dans votre terminal, ce qui installera ESLint en tant que package Angular et configurera automatiquement votre projet pour l'utiliser :

```npm
ng add @angular-eslint/schematics
```

En exécutant cette commande dans le terminal de votre projet Angular, ESLint sera installé et configuré automatiquement. Les fichiers de configuration nécessaires seront ajoutés ( .eslintrc.json), et les fichiers de configuration Angular existants seront modifiés pour intégrer ESLint dans votre projet.

![ng_add__angular-eslint](uploads/5c07797e409d161d70bb36d6c61c0fe0/ng_add__angular-eslint.png)

### Configuration

ESLint est hautement configurable, vous permettant d'adapter ses règles à votre projet. Après avoir ajouté ESLint à votre projet Angular, vous pouvez personnaliser son comportement en modifiant le fichier .eslintrc.json à la racine de votre projet. Voici un exemple de configuration de base que vous pouvez personnaliser selon les besoins de votre projet :

![eslintrc](uploads/da20a068da6a399a421afdcfeab70b72/eslintrc.png)

Dans ce fichier, pour les fichiers TypeScript(\`**.ts**\`) :

- **extends :** Cette section vous permet d'étendre les configurations ESLint existantes. Dans l'exemple ci-dessus, nous avons utilisé **"eslint:recommended"** pour les règles ESLint recommandées, **"plugin :@typescript-eslint/recommended"** pour les règles recommandées pour TypeScript, et **"plugin :@angular-eslint "** pour les règles spécifiques à Angular.
- **rules :** C'est ici que vous pouvez personnaliser les règles ESLint. Vous pouvez ajouter, modifier ou supprimer des règles selon les exigences de votre projet. Par exemple, si vous souhaitez autoriser les déclarations de variables non utilisées, vous pouvez ajouter , **"no-unused-vars:off"** .

Pour les fichier **HTML :**

- **extends** : Cette propriété indique les configurations ESLint étendues que vous utilisez pour les fichiers HTML. Dans cet exemple :
  - **"plugin :@angular-eslint/template/recommended"** étend la configuration recommandée pour les fichiers HTML d'Angular. Cela signifie qu'il appliquera les règles recommandées pour les fichiers HTML selon les normes d'Angular.
  - **"plugin :@angular-eslint/template/accessibility"** étend également la configuration d'accessibilité recommandée pour les fichiers HTML d'Angular. Cela garantit que les fichiers HTML de votre projet respectent les meilleures pratiques en matière d'accessibilité.
- **rule :** Ici, aucune règle spécifique n'est définie

Ajoutons une règle qui va interdire l'utilisation du type **any** grace à la règle **"@typescript-eslint/no-explicit-any"**

![any_rules](uploads/6f7c305bca407326d1c3776c1658428d/any_rules.png)

Après avoir définie une nouvelle règle, on peut la tester grâce à la commande \*\*ng lint \*\*:

![test_no_any_type](uploads/2dfecfbc58d08e8eab1ecdc94b59d5fb/test_no_any_type.png)

**NB : Toutefois, vous pouvez ajouter le fichier .eslintignore. Il est utilisé pour indiquer à ESLint quels fichiers ou répertoires doivent être ignorés lors de l'analyse du code. Cela peut être utile si vous avez des fichiers générés automatiquement, des fichiers de configuration ou d'autres fichiers non essentiels pour l'analyse du code, et que vous ne souhaitez pas qu'ESLint les vérifie.**

En incluant un fichier **.eslintignore** dans votre projet, vous pouvez réduire le temps d'exécution d'ESLint en évitant d'analyser des fichiers inutiles, et vous pouvez également éviter les faux positifs en excluant des fichiers qui peuvent déclencher des erreurs ou des avertissements inutiles.

Voici un exemple de ce à quoi peut ressembler un fichier **.eslintignore** :

![eslintignore](uploads/fb55693729cb7e5cf75a6cbfe3fd4563/eslintignore.png)

Dans cet exemple, les lignes indiquent à ESLint d'ignorer les fichiers et répertoires correspondant à ces modèles. Les fichiers **.html** au niveau de la racine du projet (comme **index.html**), ainsi que les fichiers **index.\*.html** dans tous les répertoires, et les fichiers **.html** dans le répertoire **assets/** seront ignorés lors de l'analyse du code.

De plus, les répertoires **.vscode/**, **dist/**, **assets/** et **node_modules/** seront également ignorés, ce qui est courant car ce sont des répertoires générés automatiquement ou contenant des dépendances externes.

En ajoutant ces éléments à votre fichier **.eslintignore**, vous indiquez à ESLint de ne pas vérifier ces fichiers ou répertoires lors de l'analyse du code, ce qui peut améliorer l'efficacité de l'outil et réduire le bruit dans les résultats de l'analyse.

## Prettier

### C'est quoi prettier?

Prettier est un outil de formatage de code qui vous permet de maintenir un style de code cohérent dans votre projet. Contrairement à ESLint qui se concentre sur la détection des erreurs, Prettier se concentre uniquement sur le formatage du code, garantissant ainsi que votre code a toujours une apparence propre et uniforme.

### Installation

Pour installer Prettier dans votre projet, exécutez la commande suivante dans votre terminal :

```npm
npm install prettier --save-dev
```

### Configuration

Prettier est hautement configurable. Vous pouvez personnaliser son comportement en créant un fichier de configuration **.prettierrc** à la racine de votre projet. Voici un exemple de configuration :

![prettier_setting](uploads/9e8365fc5cc5eb8fc897d39561a91ff2/prettier_setting.png)

Dans cet exemple, les règles spécifient que le code doit utiliser des espaces au lieu de tabulations (**"useTabs": false**), utiliser des guillemets simples (**"singleQuote": true**), inclure des points-virgules à la fin des déclarations (**"semi": true**), et ainsi de suite

## Combinaison ESLint & Prettier

L'objectif de la combinaison de ces deux outils est de configurer Prettier pour qu'il soit utilisé comme plugin ESLint. Lorsque Prettier est utilisé comme plugin ESLint, il est intégré directement dans le processus d'analyse ESLint, garantissant que le formatage du code est conforme aux règles de Prettier. Voici comment configurer Prettier comme plugin ESLint.

### Installation des dépendances nécessaires

Ici vous aurez besoin des dépendances suivantes : **prettier-eslint**, **eslint-config-prettier** et **eslint-plugin-prettier**

```npm
npm install prettier-eslint eslint-config-prettier eslint-plugin-prettier
```

### Configurer ESLint pour utiliser Prettier

Pour intégrer Prettier dans le processus d'analyse ESLint, il faut ajouter **"plugin:prettier/recommended"** à la section **'extends'** de notre configuration ESLint (**.eslintrc.json**). En incluant **"plugin:prettier/recommended"** dans cette liste, nous assurons que le formatage du code sera automatiquement aligné sur les règles définies par Prettier. Cela garantit un style de code uniforme et cohérent dans l'ensemble du projet, en conformité avec les standards de qualité préétablis.

![combinaison](uploads/d65d36477917fb52d57c5f13e9658cce/combinaison.png)

### Vérification de l'intégration de ESLint et Prettier

Pour confirmer l'intégration fluide d'ESLint et Prettier dans votre projet, utilisez la commande **"ng lint"**. Cette commande permet d'analyser votre code à la recherche d'erreurs liées au style du formatage défini par Prettier ainsi qu'aux règles spécifiées dans ESLint. Son exécution réussie sans erreurs ou avertissements garantit que votre code est conforme aux normes de formatage et aux règles de qualité que vous avez établies.

![check_installation](uploads/35a825d1eb24608bc7d136678d935fa8/check_installation.png)

C'est important de noter que vous avez la possibilité d'utiliser la commande **"ng lint --fix"** pour corriger automatiquement certaines des erreurs de style et de formatage détectées par ESLint. Cette commande apporte des corrections automatiques chaque fois que cela est possible, contribuant ainsi à maintenir la cohérence et la propreté du code au sein de votre projet.

## Husky

### C'est quoi Husky?

Husky est un outil de prévention des mauvais commits qui vous permet de définir des hooks Git personnalisés. Ces hooks Git sont des scripts qui s'exécutent automatiquement à des moments spécifiques du cycle de vie des commits Git, tels que juste avant qu'un commit soit créé. Husky est couramment utilisé pour exécuter des commandes de linting, de tests ou d'autres vérifications avant qu'un commit ne soit effectivement effectué.

### Installation

Pour installer Husky dans votre projet, utiliser la commande suivante :

```npm
npx husky-init && npm install
```

### Configuration

Après l'installation du paquet Husky, un hook Git pre-commit est automatiquement créé. Cependant, vous pouvez le personnaliser pour qu'il réponde spécifiquement à vos besoins. Dans notre cas, nous allons nous assurer que le commit d'un développeur sera autorisé uniquement s'il n'y a pas d'erreurs de lint et que tous les tests unitaires passent.

![pre-commit_file](uploads/7a523b7f3f3eda63a4198a1a55b050b6/pre-commit_file.png)

![](media/image10.png)

Ici \*\*test-no-watch  \*\* est un script personnalisé mappé à la commande **ng test --watch=false** .

![test-no-watch](uploads/4989ab0f2577effc85258cd1cc659d0c/test-no-watch.png)

En définissant --watch=false, vous désactivez le mode de surveillance des tests Cela signifie que les tests seront exécutés une seule fois sans attendre de nouvelles modifications de fichiers. Cela peut être utile dans des contextes tels que les scripts de pre-commit où vous voulez exécuter les tests une seule fois pour vérifier la qualité du code avant le commit, sans attendre de modifications supplémentaires. L'utilisation de --watch=false garantit que les tests unitaires seront exécutés de manière isolée, sans tenir compte des modifications de fichiers ultérieures, ce qui est particulièrement utile dans le contexte de l'intégration avec les hooks de pré-commit Git.

## Analyse basé sur le nouveau code (lint-staged)

L'analyse basée sur le nouveau code, également connue sous le nom de lint-staged, permet d'exécuter des vérifications de linting et de formatage uniquement sur les fichiers modifiés avant leur ajout à un commit. Cela permet de s'assurer que seuls les fichiers modifiés respectent les normes de qualité définies, évitant ainsi des erreurs potentielles dans le code source.

### Installation de lint-staged

Pour intégrer lint-staged à votre projet, exécutez la commande suivante dans votre terminal :

```bash
npm install lint-staged --save-dev
```

### Configuration de lint-staged

Ajoutez une section "lint-staged" à votre fichier `package.json` pour définir les vérifications à effectuer sur les fichiers modifiés. Voici un exemple de configuration qui exécute ESLint et Prettier uniquement sur les fichiers modifiés avant un commit :

```json
"lint-staged": {
  "*.{ts,html}": [
    "eslint --fix",
    "prettier --write"
  ]
}
```

Dans cet exemple :
- `"*.{ts,html}"` spécifie les types de fichiers à inclure dans le processus lint-staged. Dans ce cas, les fichiers TypeScript et HTML sont pris en compte.
- `"eslint --fix"` exécute ESLint avec l'option --fix pour corriger automatiquement les erreurs de linting lorsque cela est possible.
- `"prettier --write"` utilise Prettier pour formater les fichiers modifiés.

### Intégration avec Husky

Modifiez le fichier pre-commit pour inclure lint-staged avant chaque commit.

```sh
#!/usr/bin/env sh
. "$(dirname -- "$0")/_/husky.sh"

npx  lint-staged && npm run test-no-watch
```

Avec cette configuration, avant chaque commit, lint-staged sera exécuté pour vérifier les fichiers modifiés selon les règles définies.

### Test de lint-staged

Testez lint-staged en apportant des modifications à un fichier TypeScript ou HTML, puis effectuez un commit. Vous devriez voir lint-staged exécuter ESLint et Prettier uniquement sur les fichiers modifiés, assurant ainsi la qualité du code ajouté au dépôt.

![lint-staged-test](chemin/vers/capture/d-écran/lint-staged-test.png)

L'utilisation de lint-staged dans votre flux de travail garantit que seuls les fichiers modifiés respectent les normes de qualité avant d'être inclus dans un commit, contribuant ainsi à maintenir la cohérence et la propreté du code source de votre projet Angular.

## Conclusion
En concluant ce guide, nous avons exploré une série d'outils et de techniques essentiels pour améliorer la qualité du code dans un projet Angular. Notre parcours a débuté avec ESLint, un puissant linter JavaScript/TypeScript permettant de détecter les erreurs de syntaxe, les violations de conventions de codage et les problèmes de qualité du code. Grâce à ESLint, nous avons établi des normes de codage strictes et uniformes au sein de notre projet.

En introduisant Prettier, un outil de formatage de code, nous avons renforcé notre capacité à maintenir un style de code cohérent. Prettier nous a permis d'automatiser le processus de formatage du code, garantissant ainsi que notre code reste lisible et cohérent, indépendamment de l'auteur.

La combinaison de ESLint et Prettier a constitué une configuration puissante, alliant la détection d'erreurs de code à des règles strictes de formatage. Cette synergie a contribué à assurer la qualité, la lisibilité et la cohérence de notre code, tout en minimisant les risques d'erreurs potentielles.

Enfin, nous avons intégré ces outils dans notre flux de travail en utilisant Husky. Husky a joué un rôle essentiel en nous permettant de définir des hooks Git personnalisés, assurant ainsi qu'une vérification automatique de la qualité du code précède chaque commit. Grâce à Husky, nous avons pu bloquer les commits ne respectant pas les règles définies, préservant ainsi l'intégrité de notre base de code.

Ajoutons à cette configuration la prise en compte de lint-staged, un outil permettant d'effectuer des vérifications de linting et de formatage uniquement sur les fichiers modifiés avant leur ajout à un commit. Cette étape supplémentaire renforce notre engagement envers la qualité du code, garantissant que seuls les fichiers modifiés respectent les normes avant d'être inclus dans un commit.

En résumé, avec ESLint, Prettier, Husky, et  lint-staged, nous avons considérablement amélioré la qualité de notre code, réduit les erreurs et maintenu un niveau élevé de cohérence au sein de notre projet Angular. Ces outils demeurent des alliés essentiels pour tout développeur cherchant à produire un code robuste et professionnel. En les intégrant dans notre flux de travail, nous avons créé un environnement propice à la collaboration et à la productivité au sein de notre équipe de développement.



## Annexe

## Annexe A - Installer le plugin ESLint sur les IDE

### 1. Vscode

Pour installer le plugin ESLint dans VSCode, suivez ces étapes :
- Ouvrez VSCode.
- Accédez à l'onglet des extensions.
- Recherchez "ESLint" et installez-le.
- Redémarrez VSCode si nécessaire.

![install-eslint-plugin-vscode](uploads/969e81c1823427ad01c8354719120ccd/install-eslint-plugin-vscode.png)

### 2. Webstorm

Pour installer le plugin ESLint dans WebStorm, suivez ces étapes :
- Ouvrez WebStorm.
- Accédez à l'onglet des paramètres (Settings).
- Recherchez "ESLint" dans la barre de recherche des paramètres.
- Activez ESLint et spécifiez le chemin du fichier de configuration ESLint si nécessaire.

## Annexe B - Installer le plugin Prettier sur les IDE

### 1. Vscode

Pour installer le plugin Prettier dans VSCode, suivez ces étapes :
- Ouvrez VSCode.
- Accédez à l'onglet des extensions.
- Recherchez "Prettier" et installez-le.
- Redémarrez VSCode si nécessaire.


![install-prettier-plugin-vscode](uploads/d7f717b0dd17deb1cae89a8461ebd9aa/install-prettier-plugin-vscode.png)

### 2. Webstorm

Pour installer le plugin Prettier dans WebStorm, suivez ces étapes :
- Ouvrez WebStorm.
- Accédez à l'onglet des paramètres (Settings).
- Recherchez "Prettier" dans la barre de recherche des paramètres.
- Activez Prettier et spécifiez le chemin du fichier de configuration Prettier si nécessaire.

Ces plugins garantissent une intégration transparente d'ESLint et Prettier dans vos environnements de développement, facilitant ainsi la détection d'erreurs, le formatage automatique du code et le maintien de la qualité du code.
