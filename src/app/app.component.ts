import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent implements OnInit {
  foo = 'anything';
  foo1 = 'dumb';

  ngOnInit(): void {
    console.error('It is an error');

    // alert('foolish man')
  }

  title = 'eslintPretHus';

  // add() {}
}
