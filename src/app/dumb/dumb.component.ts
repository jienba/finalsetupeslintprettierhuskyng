import { Component } from '@angular/core';

@Component({
  selector: 'app-dumb',
  templateUrl: './dumb.component.html',
  styleUrls: ['./dumb.component.css'],
})
export class DumbComponent {
  isFoo = false;
}
